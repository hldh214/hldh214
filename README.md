### Hi there 👋

- 🔭 I’m currently working on web development.
- 🌱 I’m currently learning [English](https://pearsonpte.com/).
- 😡 I hate [ALgoRItHm](https://leetcode.com/).
- 💬 Ask me about [programming](https://github.com/hldh214), [gaming](https://steamcommunity.com/id/hldh214).
- 📫 How to reach me: hldh214@gmail.com
- 😄 Pronouns: He/Him/His
